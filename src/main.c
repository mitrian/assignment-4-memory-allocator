#include "assert.h"
#include "mem.h"
#include <stdio.h>

void test_successful_allocation();
void test_free_single_block();
void test_free_multiple_blocks();
void test_memory_extension();
void test_memory_allocation_failure();

int main() {
  test_successful_allocation();
  test_free_single_block();
  test_free_multiple_blocks();
  test_memory_extension();
  test_memory_allocation_failure();

  return 0;
}

void test_successful_allocation() {
  printf("######### Running test 1: successfull allocation #########\n");
    
  void* heap = heap_init(128);
  assert(heap);
  debug_heap(stderr, heap);

  void* ptr_1 = _malloc(64);
  assert(ptr_1);

  printf("\nBlock info:\n");
  debug_struct_info(stderr, ptr_1);

  heap_term();
  printf("######### Test 1: succeed #########\n\n");
}

void test_free_single_block() {
  printf("######### Running test 2: free single block #########\n");

  void* heap = heap_init(128);
  assert(heap);
  printf("\nInitial heap info:\n");
  debug_heap(stderr, heap);

  void* ptr_1 = _malloc(64);
  assert(ptr_1);
  printf("\nBlock 1 info:\n");
  debug_struct_info(stderr, ptr_1);

  void* ptr_2 = _malloc(32);
  assert(ptr_2);
  printf("\nBlock 2 info:\n");
  debug_struct_info(stderr, ptr_2);

  printf("\nHeap with allocated blocks info:\n");
  debug_heap(stderr, heap);

  _free(ptr_1);
    
  assert(ptr_2);

  printf("\nHeap with single block left info:\n");
  debug_heap(stderr, heap);

  heap_term();
  printf("######### Test 2: succeed #########\n\n");
}

void test_free_multiple_blocks() {
  printf("######### Running test 3: free multiple blocks #########\n");

  void* heap = heap_init(128);
  assert(heap);
  printf("\nInitial heap info:\n");
  debug_heap(stderr, heap);

  void* ptr_1 = _malloc(64);
  assert(ptr_1);
  printf("\nBlock 1 info:\n");
  debug_struct_info(stderr, ptr_1);

  void* ptr_2 = _malloc(32);
  assert(ptr_2);
  printf("\nBlock 2 info:\n");
  debug_struct_info(stderr, ptr_2);

  void* ptr_3 = _malloc(16);
  assert(ptr_2);
  printf("\nBlock 3 info:\n");
  debug_struct_info(stderr, ptr_2);

  printf("\nHeap with allocated blocks info:\n");
  debug_heap(stderr, heap);

  _free(ptr_1);
  _free(ptr_2);
    
  assert(ptr_3);

  printf("\nHeap with single block left info:\n");
  debug_heap(stderr, heap);

  heap_term();
  printf("######### Test 3: succeed #########\n\n");
}

void test_memory_extension() {
  printf("######### Running test 4: memory regions extension #########\n");
  void* heap = heap_init(0);
  assert(heap);
  printf("\nInitial heap info:\n");
  debug_heap(stderr, heap);

  void* ptr_1 = _malloc( 1024 * 1024);
  assert(ptr_1);
  printf("\nBlock 1 info:\n");
  debug_struct_info(stderr, ptr_1);

  void* ptr_2 = _malloc( 1024 * 1024);
  assert(ptr_2);
  printf("\nBlock 2 info:\n");
  debug_struct_info(stderr, ptr_2);

  printf("\nHeap with allocated blocks info:\n");
  debug_heap(stderr, heap);

  heap_term();
  printf("######### Test 4: succeed #########\n\n");
}

#define H_SIZE (1024*8)
#define H_CAP (H_SIZE - 17)
#define BLOCK_SIZE 1024

void test_memory_allocation_failure() {
  printf("######### Running test 5: memory regions allocation failure #########\n");
  void* heap = heap_init(H_CAP);
  assert(heap);
  printf("\nInitial heap info:\n");
  debug_heap(stderr, heap);

  void* ptr_1 = _malloc(H_CAP);
  assert(ptr_1);
  (void) mmap( (void*) HEAP_START + H_SIZE, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0 );
  printf("\nBlock 1 info:\n");
  debug_struct_info(stderr, ptr_1);

  void* ptr_2 = _malloc(BLOCK_SIZE);
  assert(ptr_2);
  printf("\nBlock 2 info:\n");
  debug_struct_info(stderr, ptr_2);
  debug_heap(stderr, heap);

  _free(ptr_1);
  _free(ptr_2);

  heap_term();
  printf("######### Test 5: succeed #########\n\n");
}
